<?php
########################################################################
# Extension Manager/Repository config file for ext "moox_news_extender".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'MOOX marketplace extender',
	'description' => 'Erweiterung von MOOX-News mit benutzerdefinierten Felder und Filtern',
	'category' => 'plugin',
	'author' => 'Dominic Martin',
	'author_email' => 'dm@dcn.de',
	'shy' => '',
	'dependencies' => 'moox_news',
	'conflicts' => '',
	'priority' => 'bottom',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author_company' => 'DCN GmbH',
	'version' => '0.9.1',
	'constraints' => array (
		'depends' => 
		array (
			'typo3' => '6.2.3-6.2.99',
			'moox_marketplace' => '',
		),
		'conflicts' => '',
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => '',
);

?>