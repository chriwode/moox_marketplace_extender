<?php
namespace DCNGmbH\MooxMarketplaceExtender\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 * ### GetFieldUniqueValuesViewHelper
 *
 * <ne:getFieldUniqueValues fieldname="moox_news_extender_new_field1" pageUids="1" as="newField1Values" />
 *
 * @package moox_marketplace_extender
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @subpackage ViewHelpers
 */
class GetFieldUniqueValuesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	
	/**
	 * classifiedRepository
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Repository\ClassifiedRepository
	 * @inject
	 */
	protected $classifiedRepository;
	
	/**
	 * get unique values for given
	 *
	 * @param string $fieldname
	 * @param string $pageUids page uids to load classifieds from
	 * @param string $as	 	 
	 * @return array values
	 */
	public function render($fieldname = "", $pageUids = "", $as = "") {
		
		$values = array();
		
		if($fieldname!=""){
		
			$field 			= $GLOBALS['TCA']['tx_mooxmarketplaceextender_domain_model_classified']['columns'][$fieldname];
						
			$valuesFromDb 	= $this->classifiedRepository->findUniqueValues($fieldname,$pageUids);
							
			if(count($valuesFromDb)){
								
				if($field['moox']['filterable']['select_all_label']!=""){
					$values['all'] = LocalizationUtility::translate($field['moox']['filterable']['select_all_label'], $field['extkey']);
				} else {
					$values['all'] = "Alle";
				}
				
				$valueCnt = 0;
				
				foreach($valuesFromDb AS $value){
					if($value=="empty"){
						if($field['moox']['filterable']['select_empty_label']!=""){
							$valueLabel = LocalizationUtility::translate($field['moox']['filterable']['select_empty_label'], $field['extkey']);
							$values[$value] = $valueLabel;
						} else {
							$valueLabel = "Ohne Wert";
						}
					} else {
						$values[$value] = $value;
						$valueCnt++;
					}
					
				}
				
				if($valueCnt<1){				
					unset($values);
				}
			}
		}
		
		if($as!=""){
			$this->templateVariableContainer->add($as, $values);
		} else {
			return $values;
		}
	}
}
