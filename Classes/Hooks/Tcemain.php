<?php
namespace DCNGmbH\MooxMarketplaceExtender\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_marketplace_extender
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tcemain extends DCNGmbH\MooxMarketplace\Hooks\Tcmain {

	/**
	 * do change item after saving
	 *
	 * @param object $item
	 * @return void
	 */
	public function extended_MooxMarketplaceExtender($item) {
		
		if($item->getVariant()=="moox_marketplace_extender"){
			$sortfield = $item->getMooxMarketplaceExtenderNewSelector(true);
			if($sortfield!=""){
				$sortfield = str_replace(" ","",strtolower($sortfield));		
			} else {
				$sortfield = "zzz";
			}
			$item->setMooxMarketplaceExtenderNewSelectorSortfield($sortfield);
			if($item->getMooxMarketplaceExtenderNewField2()!="" && substr($item->getMooxMarketplaceExtenderNewField2(),0,7)!="Saved: "){
				$item->setMooxMarketplaceExtenderNewField2("Saved: ".$item->getMooxMarketplaceExtenderNewField2());
				
			}
			return true;
		}
	}
}