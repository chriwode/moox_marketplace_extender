<?php
namespace DCNGmbH\MooxMarketplaceExtender\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_marketplace_extender
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Classified extends DCNGmbH\MooxMarketplace\Domain\Model\Classified {
	
	/**
	 * selectorRepository
	 *
	 * @var \DCNGmbH\MooxMarketplaceExtender\Domain\Repository\SelectorRepository
	 * @inject
	 */
	protected $selectorRepository;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewField1;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewField2;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewEmail;
	
	/**
	 * @var \int
	 */
	protected $mooxMarketplaceExtenderNewDate;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewSelector;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewCheckbox;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceExtenderNewRadio;
	
	/**	 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference>
	 * @lazy
	 */
	protected $mooxMarketplaceExtenderNewFiles;
	
	/**
	 * @var string
	 */
	protected $mooxMarketplaceExtenderNewFilterSortfield;
	
	/**
	 * Initializes all ObjectStorage properties	
	 *
	 * @return void
	 */
	public function initStorageObjects_mooxMarketplaceExtender() {		
		$this->mooxMarketplaceExtenderNewFiles = new ObjectStorage();		
	}
	
	/**
	 * Get new field 1
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewField1($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewField1!=""){
				return $this->mooxMarketplaceExtenderNewField1;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceExtenderNewField1;
		}
	}

	/**
	 * Set new field 1
	 *
	 * @param string $mooxMarketplaceExtenderNewField1 new field 1
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewField1($mooxMarketplaceExtenderNewField1) {
		$this->mooxMarketplaceExtenderNewField1 = $mooxMarketplaceExtenderNewField1;
	}
	
	/**
	 * Get new field 2
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewField2($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewField2!=""){
				return $this->mooxMarketplaceExtenderNewField2;
			} else {
				return "Keine Angabe";
			}
		} else {
			return $this->mooxMarketplaceExtenderNewField2;
		}
	}

	/**
	 * Set new field 2
	 *
	 * @param string $mooxMarketplaceExtenderNewField2 new field 2
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewField2($mooxMarketplaceExtenderNewField2) {
		$this->mooxMarketplaceExtenderNewField2 = $mooxMarketplaceExtenderNewField2;
	}
	
	/**
	 * Get new email
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewEmail($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewEmail!=""){
				return $this->mooxMarketplaceExtenderNewEmail;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceExtenderNewEmail;
		}
	}

	/**
	 * Set new email
	 *
	 * @param string $mooxMarketplaceExtenderNewEmail new email
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewEmail($mooxMarketplaceExtenderNewEmail) {
		$this->mooxMarketplaceExtenderNewEmail = $mooxMarketplaceExtenderNewEmail;
	}
	
	/**
	 * Get new date
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewDate($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewDate>0){
				return date("Y-m-d",$this->mooxMarketplaceExtenderNewDate);
			} else {
				return ""; //"Keine Angabe"
			}
		} else {
			return $this->mooxMarketplaceExtenderNewDate;
		}
	}

	/**
	 * Set new date
	 *
	 * @param string $mooxMarketplaceExtenderNewDate new date
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewDate($mooxMarketplaceExtenderNewDate) {
		$this->mooxMarketplaceExtenderNewDate = $mooxMarketplaceExtenderNewDate;
	}
	
	/**
	 * Get new selector
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewSelector($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewSelector!=""){
				$selector = $this->selectorRepository->findByUid($this->mooxMarketplaceExtenderNewSelector);
				if(is_object($selector)){
					return $selector->getTitle();
				} else {
					$items = $GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['moox_marketplace_extender_new_selector']['config']['items'];
					foreach($items AS $item){			
						if($item[1]==$this->mooxMarketplaceExtenderNewFilter){				
							$return = LocalizationUtility::translate($item[0], "moox_marketplace_extender");
							if($return==""){
								$return = $item[0];
							}
							return $return;
							break;
						}
					}
				}
				return $this->mooxMarketplaceExtenderNewSelector;
			} else {
				return ""; //"Keine Angabe"
			}
		} else {
			return $this->mooxMarketplaceExtenderNewSelector;
		}
	}

	/**
	 * Set new selector
	 *
	 * @param string $mooxMarketplaceExtenderNewSelector new selector
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewSelector($mooxMarketplaceExtenderNewSelector) {
		$this->mooxMarketplaceExtenderNewSelector = $mooxMarketplaceExtenderNewSelector;
	}
	
	/**
	 * Get new checkbox
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewCheckbox($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewCheckbox!=""){
				$values = explode(",",$this->mooxMarketplaceExtenderNewCheckbox);
				$items = $GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['moox_marketplace_extender_new_checkbox']['config']['items'];
				$return = array();
				foreach($values AS $value){
					foreach($items AS $item){			
						if($item[1]==$value){				
							$label = LocalizationUtility::translate($item[0], "moox_marketplace_extender");
							if($label==""){
								$label = $item[0];
							}
							$return[] = $label;
						}
					}
				}
				
				return (count($return))?implode(", ",$return):$this->mooxMarketplaceExtenderNewCheckbox;
			} else {
				return ""; //"Keine Angabe"
			}
		} else {
			return $this->mooxMarketplaceExtenderNewCheckbox;
		}
	}
	
	/**
	 * Set new checkbox
	 *
	 * @param string $mooxMarketplaceExtenderNewCheckbox new checkbox
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewCheckbox($mooxMarketplaceExtenderNewCheckbox) {
		$this->mooxMarketplaceExtenderNewCheckbox = $mooxMarketplaceExtenderNewCheckbox;
	}
	
	/**
	 * Get new radio
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewRadio($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceExtenderNewRadio!=""){				
				$items = $GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['moox_marketplace_extender_new_radio']['config']['items'];
				foreach($items AS $item){			
					if($item[1]==$this->mooxMarketplaceExtenderNewRadio){				
						$return = LocalizationUtility::translate($item[0], "moox_marketplace_extender");
						if($return==""){
							$return = $item[0];
						}
						return $return;
						break;
					}
				}				
				return $this->mooxMarketplaceExtenderNewRadio;
			} else {
				return ""; //"Keine Angabe"
			}
		} else {
			return $this->mooxMarketplaceExtenderNewRadio;
		}
	}

	/**
	 * Set new radio
	 *
	 * @param string $mooxMarketplaceExtenderNewRadio new radio
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewRadio($mooxMarketplaceExtenderNewRadio) {
		$this->mooxMarketplaceExtenderNewRadio = $mooxMarketplaceExtenderNewRadio;
	}
	
	/**
	 * sets files
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference> $mooxMarketplaceExtenderNewFiles
	 *
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewFiles($mooxMarketplaceExtenderNewFiles) {
		$this->mooxMarketplaceExtenderNewFiles = $mooxMarketplaceExtenderNewFiles;
	}
	 
	/**
	 * get files
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference>
	 */
	public function getMooxMarketplaceExtenderNewFiles() {
		return $this->mooxMarketplaceExtenderNewFiles;
	}

	/**
     * adds a file
     *
     * @param \DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function addMooxMarketplaceExtenderNewFiles(\DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference $file) {
        $this->mooxMarketplaceExtenderNewFiles->attach($file);
    }	
	
	/**
     * remove a file
     *
     * @param \DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function removeMooxMarketplaceExtenderNewFiles(\DCNGmbH\MooxMarketplaceExtender\Domain\Model\FileReference $file) {
        $this->mooxMarketplaceExtenderNewFiles->detach($file);
    }
	
	/**
	 * Get new selector sortfield
	 *
	 * @return string
	 */
	public function getMooxMarketplaceExtenderNewSelectorSortfield() {
		return $this->mooxMarketplaceExtenderNewSelectorSortfield;
	}

	/**
	 * Set new selector sortfield
	 *
	 * @param string $mooxMarketplaceExtenderNewSelectorSorfield new selector sortfield
	 * @return void
	 */
	public function setMooxMarketplaceExtenderNewSelectorSortfield($mooxMarketplaceExtenderNewSelectorSortfield) {
		$this->mooxMarketplaceExtenderNewSelectorSortfield = $mooxMarketplaceExtenderNewSelectorSortfield;
	}
}
