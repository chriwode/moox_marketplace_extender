<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace_extender']);

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_marketplace_extender/Resources/Private/Language/locallang.xml:';

// add new classified variant to variant selector
$GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['variant']['config']['items'][] = array('LLL:EXT:moox_marketplace_extender/Resources/Private/Language/locallang_db.xlf:tx_mooxmarketplaceextender_domain_model_classified','moox_marketplace_extender');

// hide classified default fields for new classified type 
$hideFields = "files,description";

// set tca array of new moox_marketplace elements
$tx_mooxmarketplaceextender_domain_model_classified = array(	
	'moox_marketplace_extender_new_checkbox' => Array (		
		'exclude' => 1,	
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'label' => $ll.'form.moox_marketplace_extender_new_checkbox',		
		'config' => Array (
			'type' => 'check',			
			'items' => Array (
					Array('Auswahl 1', 'a1'),
					Array('Auswahl 2', 'a2'),	
					Array('Auswahl 3', 'a3'),
			),
			'size' => 1,	
			'maxitems' => 1,
			'allowNonIdValues' => 1,
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 0
		),		
	),
	'moox_marketplace_extender_new_radio' => Array (		
		'exclude' => 1,	
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'label' => $ll.'form.moox_marketplace_extender_new_radio',		
		'config' => Array (
			'type' => 'radio',			
			'items' => Array (
					Array('Auswahl 1', 'a1'),
					Array('Auswahl 2', 'a2'),	
					Array('Auswahl 3', 'a3'),
			),
			'size' => 1,	
			'maxitems' => 1,
			'allowNonIdValues' => 1,
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 0
		),		
	),
	'moox_marketplace_extender_new_selector' => Array (		
		'exclude' => 1,	
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'label' => $ll.'form.moox_marketplace_extender_new_selector',		
		'config' => Array (
			'type' => 'select',			
			'items' => Array (
					Array('Keine Auswahl', ''),
					Array($ll.'form.moox_marketplace_extender_new_selector.static', 'static'),	
			),
			'foreign_table' => 'tx_mooxmarketplaceextender_domain_model_selector',
			'foreign_table_where' => 'AND tx_mooxmarketplaceextender_domain_model_selector.deleted=0 AND tx_mooxmarketplaceextender_domain_model_selector.hidden=0 AND tx_mooxmarketplaceextender_domain_model_selector.sys_language_uid IN (-1,0) ORDER BY tx_mooxmarketplaceextender_domain_model_selector.title ASC',
			'size' => 1,	
			'maxitems' => 1,
			'allowNonIdValues' => 1,
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 0
		),		
	),
	'moox_marketplace_extender_new_field1' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_extender_new_field1',
		'config' => array(
			'type' => 'input',
			'size' => 30,
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => array (
				'additional_sorting' => 'datetime ASC',
			),
		),		
	),
	'moox_marketplace_extender_new_field2' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_extender_new_field2',
		'config' => array(
			'type' => 'input',
			'size' => 30,
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 1,
		),
	),
	'moox_marketplace_extender_new_email' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_extender_new_email',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'email',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_extender_new_date' => array(
		'exclude' => 0,
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_extender_new_date',
		'config' => array(
			'type' => 'input',
			'size' => 7,
			'eval' => 'datetime', // or use date
			'checkbox' => 1,
		),		
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 0,			
		),		
	),
	'moox_marketplace_extender_new_files' => array(
		'exclude' => 0,
		'label' => $ll.'form.moox_marketplace_extender_new_files',		
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
			'moox_marketplace_extender_new_files',
			array(
				'reference' => 'file',
				'maxitems' => 1,
				'maxfilesize' => 20480,
				'accepts' => 'pdf',
				'uploadfolder' => ($extConf['uploadFolder']!="")?$extConf['uploadFolder']:'uploads/tx_mooxmarketplace',
				'appearance' => array(
					'headerThumbnail' => array(
						'width' => '100',
						'height' => '100',
					),
					'createNewRelationLinkTitle' => 'Datei hinzufügen',
				),
				// custom configuration for displaying fields in the overlay/reference table
				// to use the imageoverlayPalette instead of the basicoverlayPalette
				'foreign_types' => array(
					'0' => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					)
				),
			),
			'pdf'
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => array(
				"mooxmarketplace" => array(
					"add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_extender_new_selector_sortfield' => Array (		
		'exclude' => 1,	
		'label' => $ll.'form.moox_marketplace_extender_new_filter',		
		'config' => array(
			'type' => 'passthrough'
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'plugins' => 0,			
			'sortable' => 1,			
		),
	),
	'moox_marketplace_extender_header_new' => array(
		'label' => $ll.'header.moox_marketplace_extender_header_new',
		'displayCond' => 'FIELD:variant:=:moox_marketplace_extender',
		'config' => array(
			'type' => 'passthrough',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_extender',
			'header' => 1,
			'plugins' => array(
				"mooxmarketplace" => array(
					"add","edit"
				),
			),				
		),
	),
);

// extend moox_marketplace tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mooxmarketplace_domain_model_classified', $tx_mooxmarketplaceextender_domain_model_classified,1);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxmarketplace_domain_model_classified', '--div--;LLL:EXT:moox_marketplace_extender/Resources/Private/Language/locallang_db.xlf:tx_mooxmarketplaceextender_domain_model_classified.tabs.extender,moox_marketplace_extender_new_field1,moox_marketplace_extender_new_field2,moox_marketplace_extender_new_field3,moox_marketplace_extender_new_date,moox_marketplace_extender_new_selector,moox_marketplace_extender_new_checkbox,moox_marketplace_extender_new_radio,moox_marketplace_extender_new_files', '', 'after:files');

// hide classified default fields defined in hide fields string 
\DCNGmbH\MooxMarketplace\Service\HelperService::tcaHideDefaultFields($hideFields,"tx_mooxmarketplace_domain_model_classified","moox_marketplace");
?>